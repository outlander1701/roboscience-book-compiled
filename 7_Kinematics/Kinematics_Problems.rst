Problems
--------

#. Is the following constraint holonomic: :math:`\dot{x}_2\sin(x_1) + x_2 \cos(x_1)\dot{x}_1 = 0`.



#. Is the differential drive motion model given by
   :eq:`ddkinematicsmodel`  holonomic? Why or why not?
   


#. Show that the differential drive kinematic equations are non-holonomic constraints.


#. Derive :eq:`meccanumCFK`.


#. Assume that you have a square robot which is 50 cm per side and uses
   four 15 cm diameter omniwheels with\ :math:`\gamma=0` configuration. The
   wheels are mounted at each corner at :math:`45^\circ` to the sides.
   Find the kinematic equations of motion.


#. What are the motion equations for the Syncro Drive System as a function
   of wheel velocity and wheel turn angle? Use :math:`r` for wheel radius.

#. Derive the equations of motion for the three wheel omniwheel robot, :numref:`Fig:Tribot`.
