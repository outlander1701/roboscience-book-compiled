.. _`Chap:Simulation`:

****************************
Software and Simulation
****************************


With a couple of examples in hand, we can then move on to discuss using them
to model robots, robot motion and the environment.  Modeling is important
for the design process as well as software testing.  In addition, the components
of the modeling process are used in the filtering of sensor data and
the reconstruction of the robot's environment.



.. toctree::
   :maxdepth: 1

   1_SimulatingMotion
   2_TwoLinkSim
   3_Differential
   4_Interactive
   5_MovingDifferential
   6_Noise
   7_GroundRobotWorld
   8_Simulation_Problems
