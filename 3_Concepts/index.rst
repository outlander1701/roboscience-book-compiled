.. _`Chap:Terms`:

************************************
Fundamental Concepts
************************************

Getting the language down is the first step. Robotics is like any other
engineering field with lots of jargon and specialized terms. The terms
do convey important concepts which we will introduce here.

There are three examples which we will examine: the serial two
link arm, the parallel two link arm and the differential drive mobile
robot. The serial two link manipulator is a simple robot arm that has
two straight links each driven by an actuator (like a servo). They serve
as basic examples of common robotic systems and are used to introduce
some basic concepts. The parallel two link arm is a two dimensional
version of a common 3D Printer known as the Delta configuration.

We will do one detour and present a very popular formalism to describe
manipulators.  It will give a general way to mathematically describe multilink
robotic arms.  The formalism will be applied to the previous two link example
to bring the discussion full circle.  Last is
the differential drive mobile robot. This design has two drive wheels
and then a drag castor wheel. The two drive wheels can operate
independently like a skid steer “Bobcat”.

.. toctree::
   :maxdepth: 1

   1_Terms
   2_ReferenceFrames
   3_PlanarManipulators
   4_Denavit-Hartenberg
   5_Mobile
   6_Terms_Problems
